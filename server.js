const express = require('express');
const path = require('path');
const auth = require('./auth');
const app = express();

const forceSSL = function() {
  return function (req, res, next) {
    if (req.headers['x-forwarded-proto'] !== 'https') {
      return res.redirect(
       ['https://', req.get('Host'), req.url].join('')
      );
    }
    next();
  }
}

// enable this line when you want basic authentication
//app.use(auth);

app.use(express.static(__dirname + '/wwwroot'));

// To enable SSL
//app.use(forceSSL());

app.get('/', function(req, res) {
  res.sendFile(path.join(__dirname + '/wwwroot/index.html'));
});

app.listen(process.env.PORT || 8080);